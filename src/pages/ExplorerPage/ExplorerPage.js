import React from "react"
import "./ExplorerPage.css";
import {Page} from "../../components/Page";
import {ExplorerItem} from "../../components/ExplorerItem/ExplorerItem";
import {createFolder, getFolders} from "../../repositories/folderRepository"
import { getDocuments} from "../../repositories/documentRepository"
import {Navbar} from "../../components/Navbar";
import {useNavigate} from "react-router-dom";


export const ExplorerPage = () => {
    const modalRef = React.useRef()
    const navigate = useNavigate()

    const [currentFolderId, setCurrentFolderId] = React.useState(0)
    const [foldersIdHistory, setFoldersIdHistory] = React.useState([])

    const [documents, setDocuments] = React.useState([])
    const [folders, setFolders] = React.useState([])

    React.useEffect(() => {
        fetchDocumentsAndFolders(currentFolderId)
    }, [currentFolderId])

    return (
        <Page
            title="Explorateur de documents"
            onBackClicked={goToPreviousFolder}
        >
            {documents.length === 0 && folders.length === 0 && (
                <div>AUCUN REPERTOIRE</div>
            )}

            <div className="row">
                {folders.map(({id, title}) => (
                    <ExplorerItem
                        key={id}
                        id={id}
                        name={title}
                        type="folder"
                        onFolderClicked={() => navigateTo(id)}
                    />
                ))}
                {documents.map(({id, title, file}) => (
                    <ExplorerItem
                        key={id}
                        id={id}
                        name={title}
                        file={file}
                        type="file"
                    />
                ))}
            </div>
            <dialog ref={modalRef}>
                <div>
                    Créer un nouveau dossier
                    <form onSubmit={onCreateFolderSubmit}>
                        <input type="text" id="title" />
                        <button type="submit" >créer le dossier</button>
                        <button type="button" onClick={() => modalRef.current.close()}>annuler</button>
                    </form>
                </div>
            </dialog>
            <Navbar
                isRoot={currentFolderId === 0}
                onCreateFolder={() => modalRef.current.showModal()}
                onCreateDocument={() => navigate(`new/${currentFolderId}`)}
            />
        </Page>
    )

    function onCreateFolderSubmit(event) {
        event.preventDefault()

        const { title: titleInput } = event.target

        createFolder(titleInput.value, currentFolderId)
        modalRef.current.close()

        fetchDocumentsAndFolders(currentFolderId)
    }

    async function navigateTo(folderId) {
        setFoldersIdHistory((currentHistory) => [...currentHistory, currentFolderId])
        setCurrentFolderId(folderId)
    }

    async function goToPreviousFolder() {
        if(foldersIdHistory.length === 0) {
            return
        }

        const newHistory = [ ...foldersIdHistory ]
        const previousFolderId = newHistory.pop()

        setCurrentFolderId(previousFolderId)
        setFoldersIdHistory(newHistory)
    }

    async function fetchDocumentsAndFolders(folderId) {
        const explorerFolders =  getFolders(folderId)
        setFolders(explorerFolders)

        const explorerDocuments = getDocuments(folderId)
        setDocuments(explorerDocuments)
    }
}
