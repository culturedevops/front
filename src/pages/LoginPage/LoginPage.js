import './LoginPage.css'
import {useNavigate, useParams} from "react-router-dom";
import {Page} from "../../components/Page";

export function LoginPage() {
    const {id} = useParams()
    const navigate = useNavigate()


    return (
        <Page
            title="Portail de connexion admin"
            onBackClicked={() => navigate(-1)}
        >
            <h4>Bienvenue ! Connexion à votre compte</h4>
            <form className="Form" onSubmit={handleSubmit}>
                <input type="text" id="email" name="email" placeholder="Email"/>
                <input type="password" id="pwd" name="password" placeholder="Mot de passe"/>
                <input type="submit" value="Se connecter"/>
            </form>
        </Page>
    )

    function handleSubmit(event) {
        event.preventDefault()
        const email = event.target[0].value
        const password = event.target[1].value
        //Call a l'api quand ca marchera
        localStorage.setItem('isAdmin', true);
        alert("Vous passez en mode Admin");
        navigate(-1)
    }
}
