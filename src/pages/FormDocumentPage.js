import './FormPage.css'
import {useNavigate, useParams} from "react-router-dom"
import {Page} from "../components/Page"
import {createDocument} from "../repositories/documentRepository";

export default function FormDocumentPage() {
    const { id, folderId } = useParams()
    const navigate = useNavigate()

    return (
        <Page
            title="Formulaire de document"
            onBackClicked={() => navigate(-1)}
        >
            <div class="d-flex justify-content-center align-content-center">
                <form className="Form" onSubmit={handleSubmit} >
                    <input type="text" id="title" name="title" placeholder="Titre du fichier" />
                    <input type="file" id="file" name="file" />
                    <label htmlFor="published">Publié</label>
                    <input type="checkbox" name="published"/>
                    <textarea id="description" name="description" rows="5" cols="33" placeholder="Description"/>
                    <input type="submit" value="Envoyer" />
                </form>
            </div>
        </Page>
    )

    function handleSubmit(event) {
        event.preventDefault()

        const {
            title: titleInput,
            file: fileInput,
            description: descriptionInput,
            published: publishedInput,
        } = event.target

        // check pdf file
        if(fileInput.files.length){
            const payload = {
                title: titleInput.value,
                file: fileInput.files[0],
                description: descriptionInput.value,
                published: publishedInput.value === "on"
            }
            createDocument(payload, folderId)
        }
    }

    function checkFile(file) {
        const { size, type } = file
        return type==="application/pdf" || size<= 50000
    }
}
