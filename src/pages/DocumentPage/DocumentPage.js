import React from "react"
import {useNavigate, useParams} from "react-router-dom";
import {Page} from "../../components/Page";
import {deleteDocument, getDocumentById} from "../../repositories/documentRepository";
// import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
// import {faEdit, faEye, faTrash, faUsersViewfinder} from "@fortawesome/free-solid-svg-icons";

export const DocumentPage = () => {
  const {id} = useParams()
  const navigate = useNavigate()
  const document = getDocumentById(id)

  console.log(document)
  if (!document) {
    return navigate("/")
  }

  const publishedAtDate = new Intl.DateTimeFormat('fr-FR').format(new Date(document.published_at))
  const createdAtDate = new Intl.DateTimeFormat('fr-FR').format(new Date(document.updated_at))

  return (
    <Page onBackClicked={() => navigate(-1)}>
      <div className="container m-2 d-flex gap-4 align-content-center justify-content-center">
        {
          document.file.extension === "mp4" ? "png" :
            <img src="https://picsum.photos/500/500" alt=""/>
        }
        <div className="p-4">
          <h1 className="fst-normal">{document.title}</h1>
          <p className="fst-italic text-break">{document.description}</p>
          <div className="d-flex flex-column gap-3 my-4">
            <div><span className="fw-bold fs-4">Etat : </span>{document.published ? "Publié" : "Non publié"}</div>
            <div><span className="fw-bold fs-4">Date de publication : </span>{publishedAtDate}</div>
            <div><span className="fw-bold fs-4">Date de mise à jour : </span>{createdAtDate}</div>
          </div>

          <div className="d-flex gap-1 mt-2">
            {/*<button className="btn btn-info rounded-circle"><FontAwesomeIcon icon={faEye}/></button>*/}
            {/*<button className="btn btn-danger rounded-circle" onClick={onDeleteDocument}><FontAwesomeIcon icon={faTrash}/></button>*/}
            {/*<button className="btn btn-dark rounded-circle"><FontAwesomeIcon icon={faEdit}/></button>*/}
          </div>
        </div>
      </div>
    </Page>
  )
}
