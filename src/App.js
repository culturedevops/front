import './App.css';
import { BrowserRouter, Route, Routes } from "react-router-dom"
import FormDocumentPage from "./pages/FormDocumentPage"
import { ExplorerPage } from "./pages/ExplorerPage/ExplorerPage"
import { DocumentPage } from "./pages/DocumentPage/DocumentPage"
import { LoginPage } from "./pages/LoginPage/LoginPage"

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="" element={<ExplorerPage />} />
        <Route path="/:id" element={<DocumentPage />} />
        <Route path="/edit/:id" element={<FormDocumentPage />} />
        <Route path="new/:folderId" element={<FormDocumentPage />} />
        <Route path="/admin_login" element={<LoginPage />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App;
