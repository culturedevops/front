export async function getDocuments(idDossier) {
    const data = [
        {
            "title": "Sons of Anarchy",
            "file": {
                "filename": "son_of_anarchy",
                "extension": "png",
                "file_url": "https://tomabooks.files.wordpress.com/2017/02/sons-of-anarchy-june-wedding.jpg?w=1300"
            },
            "id_parent_folder": 1,
            "description": "Le livre de la mort qui tue les gars.",
            "user_id": 324,
            "published": 1
        },
        {
            "title": "Cours de devops",
            "file": {
                "filename": "devops_cours",
                "extension": "pdf",
                "file_url": "https://moodle.ynov.com/pluginfile.php/742307/mod_resource/content/1/Cours%20Culture%20DevOps.pdf"
            },
            "id_parent_folder": 1,
            "description": "Le livre de la mort qui tue les gars.",
            "user_id": 324,
            "published": 0
        }
    ]

    const data2 = [
        {
            "title": "Sons of Anarchy 2",
            "file": {
                "filename": "son_of_anarchy",
                "extension": "png",
                "file_url": "https://tomabooks.files.wordpress.com/2017/02/sons-of-anarchy-june-wedding.jpg?w=1300"
            },
            "id_parent_folder": 1,
            "description": "Le livre de la mort qui tue les gars.",
            "user_id": 324,
            "published": 1
        },
        {
            "title": "Cours de devops 2",
            "file": {
                "filename": "devops_cours",
                "extension": "pdf",
                "file_url": "https://moodle.ynov.com/pluginfile.php/742307/mod_resource/content/1/Cours%20Culture%20DevOps.pdf"
            },
            "id_parent_folder": 1,
            "description": "Le livre de la mort qui tue les gars.",
            "user_id": 324,
            "published": 0
        }
    ]

    const data3 = [
        {
            "title": "Sons of Anarchy 2",
            "file": {
                "filename": "son_of_anarchy",
                "extension": "png",
                "file_url": "https://tomabooks.files.wordpress.com/2017/02/sons-of-anarchy-june-wedding.jpg?w=1300"
            },
            "id_parent_folder": 1,
            "description": "Le livre de la mort qui tue les gars.",
            "user_id": 324,
            "published": 1
        },
        {
            "title": "Cours de devops 2",
            "file": {
                "filename": "devops_cours",
                "extension": "pdf",
                "file_url": "https://moodle.ynov.com/pluginfile.php/742307/mod_resource/content/1/Cours%20Culture%20DevOps.pdf"
            },
            "id_parent_folder": 1,
            "description": "Le livre de la mort qui tue les gars.",
            "user_id": 324,
            "published": 0
        }
    ]



    const folders = [
        data,
        data2,
        data3,
    ]
    // const response = await fetch(`/documents?` + idDossier, {
    //     method: 'GET',
    //     headers: {'Content-Type': 'application/json'},
    //     // Dev
    //      body: JSON.stringify(data)
    // })
    return await folders[idDossier - 1] ?? [];
}

export async function getFolders(idDossier) {
    const folders1 =  [
        {
            id: 2,
            title: "Dossier 1",
            id_parent_folder: 1,
        },
    ]

    const folders2 =  [
        {
            id: 3,
            title: "Dossier 3",
            id_parent_folder: 2,
        }
    ]

    const allFolders = [
        folders1,
        folders2
    ]

    return await allFolders[idDossier - 1] ?? []
}

export async function createOneDirectory(title, idParentFolder) {

    const response = await fetch(`/folder`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: {
            title,
            'id_parent_folder': idParentFolder
        }
    })
    return await response.json();
}

export async function createOneFile(title, file, idParentFile, description, userId) {

    const response = await fetch(`/upload`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: {
            title,
            file,
            'id_parent_folder': idParentFile,
            description,
            'user_id': userId
        }
    })
    return await response.json();
}
