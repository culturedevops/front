export function getFolders(folderId) {
    const folders = getFoldersFromStorage()

    if(folderId) {
        return folders.filter(folder => folder.id_parent_folder === folderId)
    }

    return folders
}

export function createFolder(title, folderId) {
    const folders  = getFolders()

    folders.push({
        id: folders.length + 1,
        title,
        id_parent_folder: folderId
    })

    setFoldersToStorage(folders)
}

function getFoldersFromStorage() {
    return JSON.parse(localStorage.getItem("folders")) ?? []
}

function setFoldersToStorage(folders) {
    localStorage.setItem("folders", JSON.stringify(folders))
}
