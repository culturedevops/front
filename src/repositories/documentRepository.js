export function getDocuments(folderId) {
    const documents = getDocumentsFromStorage()
    
    if (folderId !== undefined) {
        return documents.filter(doc => +doc.id_parent_folder === +folderId)
    }

    return documents
}

export function getDocumentById(documentId) {
    const documents = getDocumentsFromStorage()

    return documents.find(doc => +doc.id === +documentId)
}

export function createDocument(document, folderId) {
    const documents = getDocuments()

    documents.push({
        id: documents.length + 1,
        title: document.title,
        file: {
            filename: document.file.filename,
            file_url: document.file.url,
            extension: "png"
        },
        id_parent_folder: folderId,
        description: document.description,
        published: Boolean(document.published),
        updated_at: new Date(),
        published_at: document.published ? new Date() : null
    })
    console.log(documents)
    setDocumentsToStorage(documents)
}

function getDocumentsFromStorage() {
    return JSON.parse(localStorage.getItem("documents")) ?? []
}

function setDocumentsToStorage(documents) {
    localStorage.setItem("documents", JSON.stringify(documents))
}
