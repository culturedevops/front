import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";

export const Page = (props) => (
  <div className="d-flex flex-column page p-4">
    <div className="container flex-grow-1">
      {props.title && <h1>{props.title}</h1>}
      <button className="back-button" onClick={props.onBackClicked}>
        <FontAwesomeIcon icon={faArrowLeft}/>
      </button>
      {props.children}
    </div>
  </div>
)
