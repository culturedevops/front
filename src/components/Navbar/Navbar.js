import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFile, faFolder, faUser} from "@fortawesome/free-solid-svg-icons";
import "./Navbar.css";
import {useNavigate} from "react-router-dom";

export const Navbar = (props) => {
  const navigate = useNavigate()

  return (
    <nav className="navbar d-flex align-content-center justify-content-center">
      {!props.isRoot && (
        <button onClick={() => props.onCreateDocument()}><FontAwesomeIcon icon={faFile}/></button>
      )}
      <button onClick={() => props.onCreateFolder()}><FontAwesomeIcon icon={faFolder}/></button>
      <button onClick={() => navigate("/admin_login")}><FontAwesomeIcon icon={faUser}/></button>
    </nav>
  );
}
