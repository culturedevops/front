import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFile, faFolder} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";

export const ExplorerItem = (props) => {
    const {id, name, type } = props

    if (type === "folder") {
        return (
            <div
                onClick={props.onFolderClicked}
                className="col flex-column text-center element w-50 h-50"
            >
                <FontAwesomeIcon icon={faFolder} size="2xl"/>
                <div>{name}</div>
            </div>
        )
    }

    return (
        <Link
            to={id.toString()}
            className="col flex-column text-center element w-50 h-50 text-decoration-none"
        >
            <FontAwesomeIcon icon={faFile} size="2xl"/>
            <div>{name}</div>
        </Link>
    );
}
